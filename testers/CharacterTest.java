import character.classes.Mage;
import character.classes.Ranger;
import character.classes.Rogue;
import character.classes.Warrior;
import character.exceptions.InvalidArmorException;
import character.exceptions.InvalidWeaponException;
import items.Armor;
import items.enums.ArmorType;
import items.enums.WeaponType;
import items.Slot;
import items.Weapon;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import character.Character;
import character.*;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {

    /**
     * Level up tests.
     */
    @Test
    void TestCharacterInstantiate_Character_ShouldBeCorrectLevel() {
        Character characterMage = new Mage("Mage");
        Assertions.assertEquals(1, characterMage.getLevel());
    }

    @Test
    void TestCharacterLevelUp_Character_ShouldBeCorrectLevel() {
        Character characterMage = new Mage("Mage");
        Assertions.assertEquals(2, characterMage.levelUp(1));
    }

    /**
     * Instantiate attributes tests.
     */
    @Test
    void TestCharacterInstantiate_Mage_ShouldHaveRightAttributes() {
        Character character = new Mage("character");

        Assertions.assertEquals(1, character.getTotalPrimaryAttributes().getStrength());
        Assertions.assertEquals(1, character.getTotalPrimaryAttributes().getDexterity());
        Assertions.assertEquals(8, character.getTotalPrimaryAttributes().getIntelligence());
    }

    @Test
    void TestCharacterInstantiate_Warrior_ShouldHaveRightAttributes() {
        Character character = new Warrior("character");

        Assertions.assertEquals(5, character.getTotalPrimaryAttributes().getStrength());
        Assertions.assertEquals(2, character.getTotalPrimaryAttributes().getDexterity());
        Assertions.assertEquals(1, character.getTotalPrimaryAttributes().getIntelligence());
    }

    @Test
    void TestCharacterInstantiate_Ranger_ShouldHaveRightAttributes() {
        Character character = new Ranger("character");

        Assertions.assertEquals(1, character.getTotalPrimaryAttributes().getStrength());
        Assertions.assertEquals(7, character.getTotalPrimaryAttributes().getDexterity());
        Assertions.assertEquals(1, character.getTotalPrimaryAttributes().getIntelligence());
    }

    @Test
    void TestCharacterInstantiate_Rogue_ShouldHaveRightAttributes() {
        Character character = new Rogue("character");

        Assertions.assertEquals(2, character.getTotalPrimaryAttributes().getStrength());
        Assertions.assertEquals(6, character.getTotalPrimaryAttributes().getDexterity());
        Assertions.assertEquals(1, character.getTotalPrimaryAttributes().getIntelligence());
    }

    /**
     * Character level up attributes tests.
     */
    @Test
    void TestCharacterLevelUp_Mage_AttributesShouldIncreaseCorrectly() {
        Character character = new Mage("character");
        character.levelUp(1);

        Assertions.assertEquals(2, character.getTotalPrimaryAttributes().getStrength());
        Assertions.assertEquals(2, character.getTotalPrimaryAttributes().getDexterity());
        Assertions.assertEquals(13, character.getTotalPrimaryAttributes().getIntelligence());
    }

    @Test
    void TestCharacterLevelUp_Warrior_AttributesShouldIncreaseCorrectly() {
        Character character = new Warrior("character");
        character.levelUp(1);

        Assertions.assertEquals(8, character.getTotalPrimaryAttributes().getStrength());
        Assertions.assertEquals(4, character.getTotalPrimaryAttributes().getDexterity());
        Assertions.assertEquals(2, character.getTotalPrimaryAttributes().getIntelligence());
    }

    @Test
    void TestCharacterLevelUp_Ranger_AttributesShouldIncreaseCorrectly() {
        Character character = new Ranger("character");
        character.levelUp(1);

        Assertions.assertEquals(2, character.getTotalPrimaryAttributes().getStrength());
        Assertions.assertEquals(12, character.getTotalPrimaryAttributes().getDexterity());
        Assertions.assertEquals(2, character.getTotalPrimaryAttributes().getIntelligence());
    }

    @Test
    void TestCharacterLevelUp_Rogue_AttributesShouldIncreaseCorrectly() {
        Character character = new Rogue("character");
        character.levelUp(1);

        Assertions.assertEquals(3, character.getTotalPrimaryAttributes().getStrength());
        Assertions.assertEquals(10, character.getTotalPrimaryAttributes().getDexterity());
        Assertions.assertEquals(2, character.getTotalPrimaryAttributes().getIntelligence());
    }

    /**
     * Equipment tests.
     */
    @Test
    void TestCharacterEquipment_Armor_ShouldReturnTrue() throws InvalidWeaponException, InvalidArmorException {
        Character character = new Rogue("character");
        Armor armor = new Armor("Tester armor", 1, ArmorType.Leather, new PrimaryAttributes(1, 1, 1), Slot.Head);
        Assertions.assertTrue(character.equipItem(armor));
    }

    @Test
    void TestCharacterEquipment_Weapon_ShouldReturnTrue() throws InvalidWeaponException, InvalidArmorException {
        Character character = new Rogue("character");
        Weapon weapon = new Weapon("Tester weapon", 1, WeaponType.Dagger, 5, 2);
        assertTrue(character.equipItem(weapon));
    }

    @Test
    void TestCharacterDPS_NoWeapon_ShouldPass() {
        Character character = new Warrior("character");
        assertEquals(1*(1 + (5f / 100)), character.getDPS());
    }

    @Test
    void TestCharacterDPS_Weapon_ShouldPass() throws InvalidWeaponException, InvalidArmorException {
        Character character = new Warrior("character");
        character.equipItem(new Weapon("Tester weapon", 1, WeaponType.Axe, 7, 1.1f));
        assertEquals((7 * 1.1f)*(1 + (5f / 100)), character.getDPS());
    }

    @Test
    void TestCharacterDPS_WeaponAndArmor_ShouldPass() throws InvalidWeaponException, InvalidArmorException {
        Character character = new Warrior("character");
        character.equipItem(new Weapon("Tester weapon", 1, WeaponType.Axe, 7, 1.1f));
        character.equipItem(new Armor("Tester armor", 1, ArmorType.Plate, new PrimaryAttributes(1, 1, 1), Slot.Body));
        assertEquals((7 * 1.1f)*(1 + ((5f + 1f) / 100)), character.getDPS());
    }

    @Test
    void TestCharacterEquip_WeaponLevel_ShouldThrowException() {
        Character character = new Warrior("character");
        Weapon weapon = new Weapon("Tester weapon", 2, WeaponType.Axe, 7, 1.1f);
        assertThrows(InvalidWeaponException.class, () -> character.equipItem(weapon));
    }

    @Test
    void TestCharacterEquip_WeaponType_ShouldThrowException() {
        Character character = new Warrior("character");
        Weapon weapon = new Weapon("Tester weapon", 1, WeaponType.Dagger, 7, 1.1f);
        assertThrows(InvalidWeaponException.class, () -> character.equipItem(weapon));
    }

    @Test
    void TestCharacterEquip_ArmorLevel_ShouldThrowException() {
        Character character = new Warrior("character");
        Armor armor = new Armor("Tester armor", 2, ArmorType.Plate, new PrimaryAttributes(1, 1, 1), Slot.Body);
        assertThrows(InvalidArmorException.class, () -> character.equipItem(armor));
    }

    @Test
    void TestCharacterEquip_ArmorType_ShouldThrowException() {
        Character character = new Warrior("character");
        Armor armor = new Armor("Tester armor", 1, ArmorType.Cloth, new PrimaryAttributes(1, 1, 1), Slot.Body);
        assertThrows(InvalidArmorException.class, () -> character.equipItem(armor));
    }
}