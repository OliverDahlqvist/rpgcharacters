# RPGCharacters - Java Application
This project was created as part of an assignment in Java. 
The application can create characters with four different classes which all have unique properties. 
Most of the public functions run through JUnit tests to verify expected outputs. 
The application uses polymorphism to calculate the damage based on unique primary stats in each different "RPG-class".

## Installation
To use this project, either fork or clone this repository.

Once the project is downloaded you need to open the project with your preferred Java-development environment.
To run the application you have to first compile it.

## Usage
#### Main application
At the moment there is no gameplay loop. The main method creates a new character with a weapon and armor.
Here is a quick rundown of the most meaningful classes:
- `Character` is an abstract class which can be extended from to create unique character classes. 
- `CharacterPrinter` is used to display characters and equipment in the console. 
- `Item` is an abstract class which can be extended from to create different kinds of items. Items are equitable by characters.
- `PrimaryAttributes` stores the primary attributes that a character possesses. These 'stats' affect the character's damage.

The branch `side` has a basic gameplay loop.
#### Tests
To perform the JUnit tests run the `CharacterTest` file in the IDE. 19 test cases have been constructed to test
the functionality of the characters and their associated properties. For instance: to check whether a certain class gains the right amount
of attributes when leveling up.

## Contributors
Oliver Dahlqvist [@OliverDahlqvist](https://gitlab.com/OliverDahlqvist)
