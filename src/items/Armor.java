package items;

import character.PrimaryAttributes;
import items.enums.ArmorType;

public class Armor extends Item {

    private final ArmorType armorType;

    public ArmorType getArmorType() {
        return armorType;
    }

    private final PrimaryAttributes primaryAttributes;

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public Armor(String name, int requiredLevel, ArmorType armorType, PrimaryAttributes primaryAttributes, Slot slot) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.primaryAttributes = primaryAttributes;
    }

    /**
     * Returns a formatted string with armor name, required level, armor type and attributes.
     * @return Formatted armor string.
     */
    @Override
    public String getStats() {
        return "Armor name (Req Lvl: " + getRequiredLevel() + ", Type: " + armorType + "): " + getName() + primaryAttributes;
    }
}
