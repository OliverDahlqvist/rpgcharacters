package items;

import items.enums.WeaponType;

public class Weapon extends Item {
    private final WeaponType weaponType;
    private final float DPS;

    public float getDPS() {
        return DPS;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public Weapon(String name, int requiredLevel, WeaponType weaponType, float damage, float attacksPerSecond) {
        super(name, requiredLevel, Slot.Weapon);
        this.weaponType = weaponType;
        DPS = damage * attacksPerSecond;
    }

    @Override
    public String getStats() {
        return getName() + " (Req Lvl: " + getRequiredLevel() + ", Type: " + weaponType + ")\nDPS: " + DPS;
    }
}
