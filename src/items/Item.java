package items;

public abstract class Item {
    private String name;
    private int requiredLevel;
    private final Slot slot;

    public String getName() {
        return name;
    }
    public int getRequiredLevel() {
        return requiredLevel;
    }
    public Slot getSlot() {
        return slot;
    }

    /**
     *
     * @param name
     * @param requiredLevel
     * @param slot
     */
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    /**
     * Abstract method which should be implemented on all subclasses.
     * Purpose is to display the items stats in a formatted way.
     * @return Formatted item description.
     */
    public abstract String getStats();
}
