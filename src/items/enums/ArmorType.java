package items.enums;

public enum ArmorType {
    Cloth,
    Leather,
    Mail,
    Plate
}
