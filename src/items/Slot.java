package items;

public enum Slot {
    Head,
    Body,
    Legs,
    Weapon
}
