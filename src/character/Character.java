package character;

import character.exceptions.InvalidArmorException;
import character.exceptions.InvalidWeaponException;
import items.Armor;
import items.enums.ArmorType;
import items.enums.WeaponType;
import items.Item;
import items.Slot;
import items.Weapon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public abstract class Character {
    private final String name;
    private int level = 1;

    private final PrimaryAttributes basePrimaryAttributes;
    private final PrimaryAttributes attributeGain;

    private final HashMap<Slot, Item> equipment = new HashMap<>();

    private final ArrayList<ArmorType> allowedArmors = new ArrayList<>();
    private final ArrayList<WeaponType> allowedWeapons = new ArrayList<>();


    public Character(String name, ArmorType[] allowedArmors, WeaponType[] allowedWeapons, PrimaryAttributes basePrimaryAttributes, PrimaryAttributes attributeGain) {
        this.name = name;
        this.allowedArmors.addAll(new ArrayList<>(Arrays.stream(allowedArmors).toList()));
        this.allowedWeapons.addAll(new ArrayList<>(Arrays.stream(allowedWeapons).toList()));
        this.basePrimaryAttributes = basePrimaryAttributes;
        this.attributeGain = attributeGain;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    /**
     * Levels up the character by the specified amount and increases stats.
     *
     * @param amount Amount of levels to add.
     * @return New level.
     */
    public int levelUp(int amount) {
        level += amount;
        basePrimaryAttributes.addAttributes(attributeGain, amount);
        return level;
    }

    /**
     * Attempts to equip given armor in slot.
     * @param armor Armor to equip.
     */
    public boolean equipItem(Armor armor) throws InvalidArmorException {
        boolean isAllowed = allowedArmors.contains((armor.getArmorType()));

        if (armor.getRequiredLevel() > level) {
            throw new InvalidArmorException("Level not high enough for armor.");
        } else if (!isAllowed) {
            throw new InvalidArmorException("Wrong armor type.");
        }
        equipment.put(armor.getSlot(), armor);
        return true;
    }

    /**
     * Attempts to equip given weapon in slot.
     * @param weapon Weapon to equip.
     */
    public boolean equipItem(Weapon weapon) throws InvalidWeaponException {
        boolean isAllowed = allowedWeapons.contains((weapon.getWeaponType()));

        if (weapon.getRequiredLevel() > level) {
            throw new InvalidWeaponException("Level not high enough for weapon.");
        } else if (!isAllowed) {
            throw new InvalidWeaponException("Wrong weapon type.");
        }
        equipment.put(weapon.getSlot(), weapon);
        return true;
    }

    /**
     * Gets the total primary attributes, which is the base attributes with added equipment stats.
     *
     * @return Total primary attributes.
     */
    public PrimaryAttributes getTotalPrimaryAttributes() {
        PrimaryAttributes attributes = new PrimaryAttributes(basePrimaryAttributes); // Makes sure that the total primary attributes are reset.
        equipment.forEach((slot, item) -> {
            if (item instanceof Armor) {// If the item is not a weapon
                attributes.addAttributes(((Armor) item).getPrimaryAttributes());// Cast to armor and add the value
            }
        });
        return attributes;
    }

    /**
     * Checks if the character has a weapon equipped and calculates the character's DPS
     * using the abstract method getCharacterDps.
     *
     * @return Character DPS.
     */
    public float getDPS() {
        if (equipment.get(Slot.Weapon) == null) // Conditions that must be fulfilled.
            return getCharacterDPS(1);

        Weapon weapon = (Weapon) equipment.get(Slot.Weapon);
        return getCharacterDPS(weapon.getDPS());
    }

    /**
     * Returns the equipped items.
     * @return Equipped items.
     */
    public HashMap<Slot, Item> getEquipment(){
        return equipment;
    }

    /**
     * Returns the name of the class.
     * @return Name of the class.
     */
    public String getClassName(){
        return getClass().getSimpleName();
    }

    /**
     * Abstract method that should implement the calculation for each "character, ex. Ranger, Mage",
     * taking in the primary attribute each class use for the damage calculation.
     *
     * @param weaponDPS Weapon's dps.
     * @return Should return the Weapon dps * primary attribute / 100.
     */
    protected abstract float getCharacterDPS(float weaponDPS);

}
