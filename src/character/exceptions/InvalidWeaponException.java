package character.exceptions;

public class InvalidWeaponException extends Exception {
    /**
     * Invalid armor exception which can be used to throw exceptions related to weapons.
     * @param errorMessage Message to show.
     */
    public InvalidWeaponException(String errorMessage) {
        super(errorMessage);
    }
}
