package character.exceptions;

public class InvalidArmorException extends Exception {
    /**
     * Invalid armor exception which can be used to throw exceptions related to armor.
     * @param errorMessage Message to show.
     */
    public InvalidArmorException(String errorMessage) {
        super(errorMessage);
    }
}
