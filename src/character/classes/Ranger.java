package character.classes;

import items.enums.ArmorType;
import items.enums.WeaponType;
import character.PrimaryAttributes;
import character.Character;

public class Ranger extends Character {
    public Ranger(String name) {
        super(name,
                new ArmorType[] {
                        ArmorType.Mail,
                        ArmorType.Leather
                },
                new WeaponType[]{
                        WeaponType.Bow
                },
                new PrimaryAttributes(1, 7, 1),
                new PrimaryAttributes(1, 5, 1));
    }

    /**
     * Returns the character's weapon DPS multiplied by the class primary attribute.
     * @param weaponDPS Weapon's dps.
     * @return Weapon DPS multiplied by the class primary attribute.
     */
    @Override
    public float getCharacterDPS(float weaponDPS) {
        return weaponDPS * (1 + (float)getTotalPrimaryAttributes().getDexterity() / 100);
    }
}
