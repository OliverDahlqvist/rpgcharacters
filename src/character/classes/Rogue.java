package character.classes;

import items.enums.ArmorType;
import items.enums.WeaponType;
import character.PrimaryAttributes;
import character.Character;

public class Rogue extends Character {
    public Rogue(String name) {
        super(name,
                new ArmorType[]{
                        ArmorType.Mail,
                        ArmorType.Leather
                },
                new WeaponType[]{
                        WeaponType.Dagger,
                        WeaponType.Sword
                },
                new PrimaryAttributes(2, 6, 1),
                new PrimaryAttributes(1, 4, 1));
    }

    /**
     * Returns the character's weapon DPS multiplied by the class primary attribute.
     * @param weaponDPS Weapon's dps.
     * @return Weapon DPS multiplied by the class primary attribute.
     */
    @Override
    public float getCharacterDPS(float weaponDPS) {
        return weaponDPS * (1 + (float)getTotalPrimaryAttributes().getDexterity() / 100);
    }
}
