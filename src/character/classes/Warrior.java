package character.classes;

import items.enums.ArmorType;
import items.enums.WeaponType;
import character.PrimaryAttributes;
import character.Character;

public class Warrior extends Character {
    public Warrior(String name) {
        super(name,
                new ArmorType[]{
                        ArmorType.Mail,
                        ArmorType.Plate
                },
                new WeaponType[]{
                        WeaponType.Axe,
                        WeaponType.Hammer,
                        WeaponType.Sword
                },
                new PrimaryAttributes(5, 2, 1),
                new PrimaryAttributes(3, 2, 1));
    }

    /**
     * Returns the character's weapon DPS multiplied by the class primary attribute.
     * @param weaponDPS Weapon's dps.
     * @return Weapon DPS multiplied by the class primary attribute.
     */
    @Override
    public float getCharacterDPS(float weaponDPS) {
        return weaponDPS * (1 + (float)getTotalPrimaryAttributes().getStrength() / 100);
    }
}
