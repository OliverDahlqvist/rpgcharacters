package character.classes;

import items.enums.ArmorType;
import items.enums.WeaponType;
import character.PrimaryAttributes;
import character.Character;

public class Mage extends Character {
    public Mage(String name) {
        super(name,
                new ArmorType[] {
                        ArmorType.Cloth
                },
                new WeaponType[]{
                        WeaponType.Staff,
                        WeaponType.Wand
                },
                new PrimaryAttributes(1, 1, 8),
                new PrimaryAttributes(1, 1, 5));
    }

    /**
     * Returns the character's weapon DPS multiplied by the class primary attribute.
     * @param weaponDPS Weapon's dps.
     * @return Weapon DPS multiplied by the class primary attribute.
     */
    @Override
    public float getCharacterDPS(float weaponDPS) {
        return weaponDPS * (1 + (float)getTotalPrimaryAttributes().getIntelligence() / 100);
    }
}
