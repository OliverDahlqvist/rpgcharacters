package character;

public class PrimaryAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    public int getStrength() {
        return strength;
    }
    public int getDexterity() {
        return dexterity;
    }
    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Constructor for Primary Attributes which takes in each attribute.
     * @param strength Amount of strength.
     * @param dexterity Amount of dexterity.
     * @param intelligence Amount of intelligence.
     */
    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    /**
     * Constructor which takes in a PrimaryAttribute
     * and copies the values from the given PrimaryAttribute.
     * @param attributes Attributes to copy.
     */
    public PrimaryAttributes(PrimaryAttributes attributes) {
        strength = attributes.strength;
        dexterity = attributes.dexterity;
        intelligence = attributes.intelligence;
    }

    /**
     * Takes a primary attributes * multiplier, and adds the values to this instance's values.
     * @param primaryAttributes Attribute values to add.
     * @param multiplier Multiplies the attribute values.
     */
    public void addAttributes(PrimaryAttributes primaryAttributes, int multiplier) {
        strength += primaryAttributes.strength * multiplier;
        dexterity += primaryAttributes.dexterity * multiplier;
        intelligence += primaryAttributes.intelligence * multiplier;
    }

    /**
     * Adds each attribute value to the current instance's attribute values.
     * @param attributeToAdd Attribute to add.
     */
    public void addAttributes(PrimaryAttributes attributeToAdd) {
        strength += attributeToAdd.getStrength();
        dexterity += attributeToAdd.getDexterity();
        intelligence += attributeToAdd.getIntelligence();
    }

    /**
     * Returns all primary attributes in a formatted string.
     * @return All attributes.
     */
    public String toString() {
        return "\nStrength: " + strength + "\nDexterity: " + dexterity + "\nIntelligence: " + intelligence + "\n";
    }
}
