package printers;

import character.Character;
import items.Item;
import items.Slot;

import java.util.HashMap;

/**
 * Provides functionality to print out character stats and equipment.
 */
public class CharacterPrinter {

    /**
     * Used to display the character's stats.
     */
    public void printCharacter(Character character){
        if(character == null) return; // Return conditions

        String characterSheet = "Name: "
                + character.getName()
                + " (" + character.getClassName()
                + ")\nLevel: " + character.getLevel()
                + character.getTotalPrimaryAttributes()
                + "DPS: " + character.getDPS()
                + "\n";
        System.out.println(characterSheet);
    }

    /**
     * Displays the character's equipped items.
     */
    public void printEquipment(HashMap<Slot, Item> equipment){
        if (equipment.size() == 0) return; // Return conditions
        equipment.forEach((key, value) -> System.out.println(value.getStats()));
    }
}
