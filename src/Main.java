import character.PrimaryAttributes;
import character.classes.Rogue;
import character.Character;
import character.exceptions.InvalidArmorException;
import character.exceptions.InvalidWeaponException;
import items.Armor;
import items.Slot;
import items.Weapon;
import items.enums.ArmorType;
import items.enums.WeaponType;
import printers.CharacterPrinter;

public class Main {
    public static void main(String[] args) throws InvalidWeaponException, InvalidArmorException {
        CharacterPrinter characterPrinter = new CharacterPrinter();
        Character character = new Rogue("Test");

        Weapon weapon = new Weapon("Weapon", 1, WeaponType.Dagger, 10, 2);
        Armor armor = new Armor("Armor", 1, ArmorType.Leather, new PrimaryAttributes(10, 10, 10), Slot.Head);

        character.equipItem(weapon);
        character.equipItem(armor);

        characterPrinter.printCharacter(character);
        characterPrinter.printEquipment(character.getEquipment());
    }
}